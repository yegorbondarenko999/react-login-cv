import React from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';

import { login } from "../actions/LoginActions";
import Status from "../components/Status";

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.login(this.state.username, this.state.password);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="username" placeholder="Login" 
                 value={this.state.username} onChange={this.handleChange}>
          </input>
          <input type="password" name="password" placeholder="Password" 
                 value={this.state.password} onChange={this.handleChange}>
          </input>
          <button>Login</button>
        </form>
        <Status 
          success={this.props.logged}
          loading={this.props.loading}
          error={this.props.error}
          />
      </div>
    )
  }
}

Login.propTypes = {
  loading: PropTypes.bool,
  logged: PropTypes.bool,
  error: PropTypes.number,
  login: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    loading: state.login.loading, 
    logged: state.login.logged, 
    error: state.login.error
  };
}

const mapDispatchToProps = {
  login
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
