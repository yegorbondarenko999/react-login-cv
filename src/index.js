import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";

import Login from "./containers/Login";
import configureStore from "./store/configureStore";

const store = configureStore()

render(
	<Provider store={store}>
		<Login />
	</Provider>,
  	document.getElementById("root")
);