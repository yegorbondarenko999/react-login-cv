import React from "react";
import PropTypes from 'prop-types';

export default class Status extends React.Component {
	render() {
		if (this.props.success) {
			return <div>Success!</div>;
		} else if (this.props.loading) {
			return <div>Loading...</div>;
		} else if (this.props.error) {
			return <div>Error: {this.props.error}</div>;
		} else {
			return null;
		}
	}
}

Status.propTypes = {
  success: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.number,
};