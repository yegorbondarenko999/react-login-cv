import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILED } from "../constants/Login";

const initialState = {
  logged: false,
  error: null,
  loading: false
};

export default function login(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        logged: true,
        loading: false,
        error: null
      };

    case LOGIN_FAILED:
      return {
        ...state,
        logged: false,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
}