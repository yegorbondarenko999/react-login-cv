import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILED } from "../constants/Login";

export function login(username, password) {
  return (dispatch) => {
    dispatch({
      type: LOGIN_REQUEST
    })

    // api simulations
    setTimeout(() => {
        dispatch({
          type: LOGIN_SUCCESS
        })
    }, 1000);

    // setTimeout(() => {
    //     dispatch({
    //       type: LOGIN_FAILED,
    //       payload: 401
    //     })
    // }, 1000);
  }
}
